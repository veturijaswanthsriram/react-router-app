import React, { useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import data from "../data";

const User = () => {
	const { userId } = useParams();
	const user = data.find((user) => user.id === parseInt(userId));
	const navigate = useNavigate();

	useEffect(() => {
		document.title = "User" + userId ? userId : "none";
	}, [userId]);

	if (!user) {
		return <h2>User Not Found</h2>;
	}

	const handleBack = (e) => {
		navigate("/users");
	};

	return (
		<>
			<button onClick={handleBack}>Back</button>
			<div className="user">
				<img src="https://via.placeholder.com/150" alt="Profile pic" />
				<h2>Id:- {user.id}</h2>
				<h2>Name:- {user.name}</h2>
			</div>
		</>
	);
};

export default User;
