import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";

const Login = () => {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const navigate = useNavigate();
	const location = useLocation();
	const from = location.state?.from?.pathname || "/";

	useEffect(() => {
		document.title = "Login";
		const user = localStorage.getItem("user");
		if (user) {
			navigate(from, { replace: true });
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const handleSubmit = (e) => {
		e.preventDefault();
		localStorage.setItem("user", JSON.stringify({ username, password }));
		navigate(from, { replace: true });
	};

	return (
		<div className="login">
			<h1>Login</h1>
			<form onSubmit={handleSubmit}>
				<div className="input-group">
					<label htmlFor="username">Username</label>
					<input
						id="username"
						value={username}
						required
						onChange={(e) => setUsername(e.target.value)}
					/>
				</div>
				<div className="input-group">
					<label htmlFor="password">Password</label>
					<input
						id="password"
						value={password}
						type="password"
						required
						onChange={(e) => setPassword(e.target.value)}
					/>
				</div>
				<button>Submit</button>
			</form>
		</div>
	);
};

export default Login;
