import React, { useEffect } from "react";
import data from "../data";
import { Link, useSearchParams } from "react-router-dom";

const Users = () => {
	const [searchParams, setSearchParams] = useSearchParams();

	useEffect(() => {
		document.title = "Users";
	}, []);

	const getUsers = () => {
		const sortOrder = searchParams.get("sort_order");
		const sortBy = searchParams.get("sort_by");
		const search = searchParams.get("search");
		const dataCopy = [...data];

		if (sortOrder) {
			if (sortOrder === "asc") {
				dataCopy.sort((a, b) =>
					sortBy === "id" ? a.id - b.id : a.name.localeCompare(b.name)
				);
			} else if (sortOrder === "desc") {
				dataCopy.sort((a, b) =>
					sortBy === "id" ? b.id - a.id : b.name.localeCompare(a.name)
				);
			}
		}

		if (search) {
			return dataCopy.filter((user) =>
				user.name.toLowerCase().startsWith(search.toLowerCase())
			);
		}

		return dataCopy;
	};

	const handleChange = (e, param) => {
		const value = e.target.value;
		if (!value) {
			setSearchParams({
				...Object.fromEntries(searchParams),
				[param]: "",
			});
		} else {
			setSearchParams({
				...Object.fromEntries(searchParams),
				[param]: value,
			});
		}
	};

	return (
		<div className="users">
			<div className="users__actions">
				<div>
					<h4>Search</h4>
					<input
						value={searchParams.get("search") || ""}
						onChange={(e) => handleChange(e, "search")}
					/>
				</div>
				<div>
					<h4>Sort Order</h4>
					<select
						value={searchParams.get("sort_order") || ""}
						onChange={(e) => handleChange(e, "sort_order")}
					>
						<option value="">--</option>
						<option value="asc">ASC</option>
						<option value="desc">DESC</option>
					</select>
				</div>
				<div>
					<h4>Sort By</h4>
					<select
						value={searchParams.get("sort_by") || ""}
						onChange={(e) => handleChange(e, "sort_by")}
					>
						<option value="">--</option>
						<option value="id">id</option>
						<option value="name">name</option>
					</select>
				</div>
			</div>
			<div className="users__list">
				<h2>Users List</h2>
				{getUsers().map((user) => (
					<div key={user.id}>
						<Link to={`/user/${user.id}`}>
							{user.id} - {user.name}
						</Link>
					</div>
				))}
			</div>
		</div>
	);
};

export default Users;
