import { Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import PrivateRoute from "./components/PrivateRoute";
import GuestUser from "./pages/GuestUser";
import Home from "./pages/Home";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";
import User from "./pages/User";
import Users from "./pages/Users";

function App() {
	return (
		<>
			<Navbar />
			<main>
				<Routes>
					<Route
						path="/"
						element={
							<PrivateRoute>
								<Home />
							</PrivateRoute>
						}
					/>
					<Route path="/login" element={<Login />} />
					<Route
						path="/users"
						element={
							<PrivateRoute>
								<Users />
							</PrivateRoute>
						}
					/>
					<Route path="/user">
						<Route index element={<GuestUser />} />
						<Route
							path=":userId"
							element={
								<PrivateRoute>
									<User />
								</PrivateRoute>
							}
						/>
					</Route>
					<Route path="*" element={<NotFound />} />
				</Routes>
			</main>
		</>
	);
}

export default App;
