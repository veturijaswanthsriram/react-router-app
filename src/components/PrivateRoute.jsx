import React from "react";
import { Navigate, useLocation } from "react-router-dom";

const PrivateRoute = ({ children }) => {
	const user = localStorage.getItem("user");
	const location = useLocation();

	if (!user) {
		return (
			<Navigate to="/login" replace={true} state={{ from: location }} />
		);
	}

	return children;
};

export default PrivateRoute;
