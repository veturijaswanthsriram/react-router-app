import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";

const Navbar = () => {
	const [user, setUser] = useState(null);
	const localUser = localStorage.getItem("user");
	const navigate = useNavigate();

	useEffect(() => {
		if (localUser) {
			setUser(JSON.parse(localUser));
		}
	}, [localUser]);

	const handleLogout = (e) => {
		localStorage.removeItem("user");
		setUser(null);
		navigate("/login", { replace: true });
	};

	return (
		<nav>
			<div>
				<Link to="/">Home</Link>
			</div>
			<div>
				<Link to="/users">Users</Link>
				{user ? (
					<span onClick={handleLogout}>Logout</span>
				) : (
					<Link to="/login">Login</Link>
				)}
			</div>
		</nav>
	);
};

export default Navbar;
